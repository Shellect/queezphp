<?php
namespace core;

class Option
{

  public function __construct($id, $text, $type)
  {
    $this->id = $id;
    $this->text = $text;
    $this->type = $type;
  }

  public function render(){
    $id = $this->id;
    $text = $this->text;
    $type = $this->type;

    return <<<HTML
      <label class="form-check-label">
        <input class="form-check-input ms-2" type="$type" name="q{$id}[]">
        $text
      </label>
HTML;
  }
}
