<?php
namespace core;



class Page{

  public $questions = [];
  private $page_number;
  private $page_types = ['radio', 'checkbox', 'text'];
  private static $question_count = 0;

  public function __construct($page_number)
  {
    $this->type = $this->page_types[$page_number];
    $this->page_number = $page_number;
    $this->questions = array_map(
      ["core\Page", "createQuestion"],
      unserialize($this->readFromFile('questions.txt'))[$page_number]
    );
  }

  public  function createQuestion($text){
    $id = self::$question_count;
    $options = array_map(
      ["core\Page", "createOption"],
      unserialize($this->readFromFile('options.txt'))[$this->page_number][$id]
    );
    self::$question_count++;
    return new Question($id, $text, $options);
  }

  public function createOption($text){
    $id = 1;
    return new Option($id, $text, $this->type);
  }

  public function readFromFile($filename)
  {
    $f = fopen($filename, 'r');
    $text = fread($f, filesize($filename));
    fclose($f);
    return $text;
  }

  public function render(){
    foreach ($this->questions as $question) {
      echo $question->render();
    }
  }


}
