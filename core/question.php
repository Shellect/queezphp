<?php
namespace core;

  class Question
  {
    protected $id;
    public $text;
    public $possible_answers = [];

    function __construct($id, $text, $answers)
    {
      $this->id = $id;
      $this->text = $text;
      $this->possible_answers = $answers;
    }

    private function getOption($carry, $item){
      return $carry .= $item->render();
    }

    public function render(){
      $id = $this->id + 1;
      $text = $this->text;
      $answers = array_reduce(
        $this->possible_answers,
        [$this, "getOption"],
        ''
      );

      return <<<HTML
      <div class="my-3">
        <p>Question $id</p>
        <p>$text</p>
        $answers
      </div>
HTML;
    }
  }
