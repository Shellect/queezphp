<?php
$answers = [
    //radio
    [3, 1, 4, 1, 3, 3,],
    //checkboxes
    [
        [1,2],[1,3],[0,4],[1,4],[0,3],
        [1,2,3],[1,3],[1,2],[2,3,4],[1,3,4],
    ],
    // text
    [
        'Кубрик','Жемчуг','Гейзер','Монокль','Кактус','В зубах',
    ]
];
$serialised_data = serialize($answers);


$dir = dirname(__DIR__);
$f = fopen($dir . '/answers.txt', 'w');
fwrite($f, $serialised_data);
fclose($f);
