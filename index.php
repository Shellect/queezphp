<?php
 session_start();

 spl_autoload_register(function ($class_name) {
     $class_name = str_replace('\\', '/', $class_name);
     include $class_name . '.php';
 });
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Electronic shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/master.css">
  </head>
  <body>
    <!-- HEADER -->
    <header>

    </header>
    <!-- MAIN -->
    <main class="container">
      <div class="row">
        <div class="col-12">
          <?php
          $page_number = $_POST['page'] ?? $_SESSION['page'] ?? 0;
          $_SESSION['page'] = $page_number;
          $scores = $_SESSION['scores']??0;

          $page = new core\Page($page_number);
          $page->render();

          // $answers = unserialize($answers);
          //
          // foreach ($_POST as $key => $value) {
          //   if (preg_match('/q(\d+)/', $key, $matches)){
          //     $answers_sett = $answers[($page - 2)];
          //     $right_answer = $answers_sett[$matches[1]];
          //     if ($right_answer ==  $value){
          //        $scores += $page - 1;
          //     }
          //   }
          // }
          // $_SESSION['scores'] = $scores ?? 0;

          //include_once "views/page{$page}.php";
          ?>
        </div>
      </div>
    </main>
    <!-- FOOTER -->
    <footer></footer>

    <script src="node_modules/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="assets/js/main.js" charset="utf-8"></script>
  </body>
</html>
